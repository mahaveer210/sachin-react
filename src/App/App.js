import React from 'react';
import Button from '@material-ui/core/Button';
import CssBaseline from '@material-ui/core/CssBaseline';
import Select from '@material-ui/core/Select';
import MenuItem from '@material-ui/core/MenuItem';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';
import Container from '@material-ui/core/Container';
import Paper from '@material-ui/core/Paper';
import HelpOutlineIcon from '@material-ui/icons/HelpOutline';
import './App.css';
import initialState from '../initialState';

const useStyles = makeStyles((theme) => ({
  paper: {
    marginTop: theme.spacing(8),
    display: 'flex',
    flexDirection: 'column',
    padding:'10px 20px'
  },
  cashback:{ paddingTop: '15px',
             paddingBottom: '15px',
             borderBottom: '1px solid #ececec',
             marginLeft: '10px',
             marginRight: '10px',
             color:'#6b6b6b',
           },
  cashback_amount:{ paddingTop: '15px',
             paddingBottom: '15px',
             marginLeft: '10px',
             marginRight: '10px',
             color:'#6b6b6b'
           },
  amount:{color:'#000'},         
  form: {
    width: '100%', // Fix IE 11 issue.
    marginTop: theme.spacing(1),
  },
  submit: {
    margin: theme.spacing(3, 0, 2),
    backgroundImage: 'linear-gradient(to right, #FF6186, #F4144A)'
  },
}));

export default function App() {

  const classes = useStyles();

  const [language, setLanguage] = React.useState(initialState.en);

  const handleSetRule = (e) => {

     setLanguage(initialState[ e.target.value]);

  };

  return (
    <Container component="main" maxWidth="xs">
     <Paper>
      <CssBaseline />
      <div className={classes.paper}>
        <Typography className={classes.cashback} component="h1" variant="h5">
          Upto 5% Cashback <HelpOutlineIcon className="question_icon" />
        </Typography>

        <Typography className={classes.cashback_amount} component="h1" variant="h5">
          from <br />
           <span className={classes.amount}>{language.currency} {language.amount}</span>
        </Typography>

        <form className={classes.form} noValidate>
          
          <Select
          labelId="demo-simple-select-filled-label"
          id="demo-simple-select-filled"
          defaultValue="English"
          value={language.code}
          variant="outlined"
          margin="normal"
          required
          fullWidth
          autoComplete="current-password"
          onChange={handleSetRule}
          >
          {Object.keys(initialState).map(function(key, index) {
            return(<MenuItem value={key}>{initialState[key].language}</MenuItem>)
            })
          }  
        </Select>
         
          <Button
            type="submit"
            fullWidth
            variant="contained"
            color="primary"
            className={classes.submit}
          >
           CHECK AVAILABILITY
          </Button>
        </form>
      </div>

          <div className="rip"></div>
          
         <Grid container>
            <Grid className="footer_text" item xs> <span className="icon_100">100</span> BEST PRICE GUARANTEED </Grid>
          </Grid>

     </Paper> 
    </Container>
  );
}