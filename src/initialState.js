export default {  

 "en":
            { "code":'en', 
              "currency":'$',
             "amount":'100',
             "language":'English'},

 "de":
             { "code":'de', 
              "currency":'€', 
              "amount":'295',
             "language":'Deutsch'
             },
  "it":
             { "code":'it', 
              "currency":'AED', 
              "amount":'300',
             "language":'Italiano'
             },
}